﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using NotifyMe.Logging;

namespace NotifyMe.Forms
{
    public partial class MainForm : Form
    {

        #region Méthodes

        /// <summary>
        /// Constructeur
        /// </summary>
        public MainForm()
        {
            try
            {
                // Vérification de la présence d'une instance de l'application
                if (IsAlreadyInExecution())
                {
                    throw new ApplicationException("Une instance de l'application est déjà en cours d'exécution.");
                }

                logger = new Logger(String.Format("{0}.log", Application.ProductName));

                // Initialisation de l'interface utilisateur
                InitializeComponent();

                // Initialise la varible contenant le chemin du dernier fichier créé
                LastFileCreated = String.Empty;

                // Créé/accède à la partie du registre auquel l'application stocke ses informations
                ApplicationRegistryKey = Registry.CurrentUser.CreateSubKey(String.Format(@"SOFTWARE\{0}", Application.ProductName), RegistryKeyPermissionCheck.ReadWriteSubTree);

                // Récupère le dernier état concernant l'analyse
                bool LastStateValue = Convert.ToBoolean(ApplicationRegistryKey.GetValue("State", false));

                // Récupère le dernier état concernant l'inclusion des sous-dossiers durant l'analyse
                bool LastIncludeSubdirectoriesValue = Convert.ToBoolean(ApplicationRegistryKey.GetValue("IncludeSubdirectories", false));

                // Récupère le dernier répertoire d'analyse
                string LastPathValue = Convert.ToString(ApplicationRegistryKey.GetValue("Path", String.Empty));

                // Vérifie la validité du chemin d'analyse
                if (LastPathValue != String.Empty)
                {
                    if (Directory.Exists(LastPathValue))
                    {
                        // Affecte le dernier chemin en fonction du dernier état de l'analyse
                        FileSystemWatcher.Path = LastPathValue;

                        // Coche/décoche l'élement du menu contextuel 'Inclure les sous-dossiers'
                        IncludeSubFoldersToolStripMenuItem.Checked = LastIncludeSubdirectoriesValue;
                    }
                    else
                    {
                        throw new DirectoryNotFoundException("Le chemin d'analyse est introuvable.");
                    }
                }

                // Ouvre la clé de registre correspondant à celle qui gère le démarrage des applications au démarrage de Windows
                RunRegistryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);

                // Récupère le dernier état concernant le démarrage de l'application au démarrage de Windows
                bool LastRunAtStartupValue = RunRegistryKey.GetValue(Application.ProductName) != null;

                // Coche/décoche l'élément du menu contextuel 'Démarrer avec Windows'
                RunAtStartupToolStripMenuItem.Checked = LastRunAtStartupValue;

                // Rafraichit l'état visuel de l'application en fonction du dernier état de l'analyse
                SetState(LastStateValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                logger.Error(String.Format("{0} : {1}\r\n{2}", e.TargetSite, e.Message, e.StackTrace));

                Environment.Exit(Environment.ExitCode);
            }
        }

        /// <summary>
        /// Destructeur
        /// </summary>
        ~MainForm()
        {
            // Stoppe l'analyse en cours
            FileSystemWatcher.EnableRaisingEvents = false;
        }

        /// <summary>
        /// Méthode permettant de cacher aux yeux de l'utilisateur la fenêtre
        /// </summary>
        /// <param name="value"></param>
        protected override void SetVisibleCore(bool value)
        {
            // Cache aux yeux de l'utilisateur la fenêtre
            base.SetVisibleCore(false);
        }

        /// <summary>
        /// Ouvre l'explorateur dans le dossier contenant le dernier fichier créé
        /// </summary>
        private void OpenLastCreatedFileDirectory()
        {
            // Vérifie l'existence d'un dernier fichier créé
            if (LastFileCreated != String.Empty)
            {
                // Appelle l'explorateur
                Process.Start(Path.GetDirectoryName(LastFileCreated));

                // Rafraichit l'état visuel de l'application en fonction de l'état de l'analyse
                SetState(true);
            }
        }

        /// <summary>
        /// Rafraîchit l'état visuel de l'application en fonction de l'état de l'analyse
        /// </summary>
        /// <param name="scanState"></param>
        private void SetState(bool scanState)
        {
            // Active/désactive l'élément du menu contextuel 'Mettre en pause' en fonction du dernier état de l'analyse
            PauseScanToolStripMenuItem.Enabled = scanState;

            // Active/désactive l'élément du menu contextuel 'Démarrer' en fonction du dernier état de l'analyse
            StartScanToolStripMenuItem.Enabled = !scanState;

            // Affecte l'icone correspondant au dernier état de l'analyse
            NotifyIcon.Icon = Icon.FromHandle(scanState ? Properties.Resources.StartScan.GetHicon() : Properties.Resources.PauseScan.GetHicon());

            // Démarre/stoppe l'analyse en fonction du dernier état de l'analyse
            FileSystemWatcher.EnableRaisingEvents = scanState;
        }

        /// <summary>
        /// Vérification de la présence d'une instance de l'application
        /// </summary>
        /// <returns></returns>
        private bool IsAlreadyInExecution()
        {
            return Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Application.ExecutablePath)).Length > 1;
        }

        #endregion

        #region Evénements

        /// <summary>
        /// Evénement appelé lors d'un clic sur l'élément du menu contextuel 'Démarrer'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Affecte le chemin par défaut de la boite de dialogue
            FolderBrowserDialog.SelectedPath = FileSystemWatcher.Path;

            // Vérifie que l'utilisateur a bien validé le choix du dossier
            if (FolderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = FolderBrowserDialog.SelectedPath;

                // Affecte le nouveau chemin d'analyse
                FileSystemWatcher.Path = path;

                // Rafraichit l'état visuel de l'application en fonction de l'état de l'analyse
                SetState(true);

                // Stocke le répertoire d'analyse dans la base de registre
                ApplicationRegistryKey.SetValue("Path", path);

                // Stocke l'état concernant l'analyse dans la base de registre
                ApplicationRegistryKey.SetValue("State", true);
            }
        }

        /// <summary>
        /// Evénement appelé lors d'un clic sur l'élément du menu contextuel 'Mettre en pause'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Rafraichit l'état visuel de l'application en fonction de l'état de l'analyse
            SetState(false);

            // Stocke l'état concernant l'analyse dans la base de registre
            ApplicationRegistryKey.SetValue("State", false);
        }

        /// <summary>
        /// Evénément appelé lors d'un changement d'état de l'élément du menu contextuel 'Inclure les sous-dossier'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IncludeSubFoldersToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = IncludeSubFoldersToolStripMenuItem.Checked;

            // Active/désactive l'analyse des sous-dossiers
            FileSystemWatcher.IncludeSubdirectories = enabled;

            // Stocke l'état concernant l'inclusion des sous-dossiers durant l'analyse dans la base de registre
            ApplicationRegistryKey.SetValue("IncludeSubdirectories", enabled);
        }

        /// <summary>
        /// Evénement appelé lors d'un clic sur l'élément du menu contextuel 'Fermer'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ferme l'application
            Application.Exit();
        }

        /// <summary>
        /// Evénement appelé lors de l'apparition d'un nouveau fichier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileSystemWatcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            // Stocke le chemin du dernier fichier créé
            LastFileCreated = e.FullPath;

            // Affiche une infobulle indiquant l'apparition d'un nouveau fichier
            NotifyIcon.ShowBalloonTip(10000, "Un nouveau fichier a été créé", "Un nouveau fichier a été créé, cliquez ici pour accéder au dossier.", ToolTipIcon.Info);

            // Affecte l'icone de la barre de tâche indiquant à l'utilisateur la présence d'un nouveau fichier
            NotifyIcon.Icon = Icon.FromHandle(Properties.Resources.NewFile.GetHicon());

            // Affecte le texte info-bulle indiquant à l'utilisateur qu'un nouveau fichier à été créé
            NotifyIcon.Text = "Un nouveau fichier a été créé";
        }

        /// <summary>
        /// Evénement appelé lors d'un clic sur l'infobulle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            // Ouvre l'explorateur dans le dossier contenant le dernier fichier créé
            OpenLastCreatedFileDirectory();
        }

        /// <summary>
        /// Evénement appelé lors du clic sur l'icone de la barre de notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyIcon_MouseDown(object sender, MouseEventArgs e)
        {
            // Vérifie que le bouton appuyé est celui de gauche
            if (e.Button == MouseButtons.Left)
            {
                // Ouvre l'explorateur dans le dossier contenant le dernier fichier créé
                OpenLastCreatedFileDirectory();
            }
        }

        /// <summary>
        /// Evénement appelé lors du clic sur l'élément du menu contextuel 'Démarrer avec Windows'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunAtStartupToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (RunAtStartupToolStripMenuItem.Checked)
            {
                RunRegistryKey.SetValue(Application.ProductName, Application.ExecutablePath);
            }
            else
            {
                RunRegistryKey.DeleteValue(Application.ProductName);
            }
        }

        #endregion

        #region Propriétés

        /// <summary>
        /// Représente l'espace dans le registre auquel l'application stocke ses informations
        /// </summary>
        RegistryKey ApplicationRegistryKey;

        /// <summary>
        /// Chemin vers le dernier fichier créé
        /// </summary>
        string LastFileCreated;

        /// <summary>
        /// Clé de registre correspondant à celle qui gère le démarrage des applications au démarrage de Windows
        /// </summary>
        RegistryKey RunRegistryKey;

        /// <summary>
        /// Système de logs de l'application
        /// </summary>
        Logger logger;

        #endregion

    }
}
