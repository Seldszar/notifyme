﻿namespace NotifyMe.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FileSystemWatcher = new System.IO.FileSystemWatcher();
            this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.NotifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.StartScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PauseScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.SettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IncludeSubFoldersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RunAtStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.FileSystemWatcher)).BeginInit();
            this.NotifyIconContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // FileSystemWatcher
            // 
            this.FileSystemWatcher.EnableRaisingEvents = true;
            this.FileSystemWatcher.NotifyFilter = ((System.IO.NotifyFilters)((System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.LastWrite)));
            this.FileSystemWatcher.SynchronizingObject = this;
            this.FileSystemWatcher.Created += new System.IO.FileSystemEventHandler(this.FileSystemWatcher_Created);
            // 
            // NotifyIconContextMenuStrip
            // 
            this.NotifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StartScanToolStripMenuItem,
            this.PauseScanToolStripMenuItem,
            this.ToolStripSeparator,
            this.SettingsToolStripMenuItem,
            this.CloseToolStripMenuItem});
            this.NotifyIconContextMenuStrip.Name = "NotifyIconContextMenuStrip";
            this.NotifyIconContextMenuStrip.Size = new System.Drawing.Size(153, 120);
            // 
            // StartScanToolStripMenuItem
            // 
            this.StartScanToolStripMenuItem.Image = global::NotifyMe.Properties.Resources.StartScan;
            this.StartScanToolStripMenuItem.Name = "StartScanToolStripMenuItem";
            this.StartScanToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.StartScanToolStripMenuItem.Text = "Démarrer";
            this.StartScanToolStripMenuItem.Click += new System.EventHandler(this.StartToolStripMenuItem_Click);
            // 
            // PauseScanToolStripMenuItem
            // 
            this.PauseScanToolStripMenuItem.Image = global::NotifyMe.Properties.Resources.PauseScan;
            this.PauseScanToolStripMenuItem.Name = "PauseScanToolStripMenuItem";
            this.PauseScanToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.PauseScanToolStripMenuItem.Text = "Arrêter";
            this.PauseScanToolStripMenuItem.Click += new System.EventHandler(this.PauseToolStripMenuItem_Click);
            // 
            // ToolStripSeparator
            // 
            this.ToolStripSeparator.Name = "ToolStripSeparator";
            this.ToolStripSeparator.Size = new System.Drawing.Size(149, 6);
            // 
            // SettingsToolStripMenuItem
            // 
            this.SettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IncludeSubFoldersToolStripMenuItem,
            this.RunAtStartupToolStripMenuItem});
            this.SettingsToolStripMenuItem.Image = global::NotifyMe.Properties.Resources.Settings;
            this.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem";
            this.SettingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.SettingsToolStripMenuItem.Text = "Préférences";
            // 
            // IncludeSubFoldersToolStripMenuItem
            // 
            this.IncludeSubFoldersToolStripMenuItem.CheckOnClick = true;
            this.IncludeSubFoldersToolStripMenuItem.Name = "IncludeSubFoldersToolStripMenuItem";
            this.IncludeSubFoldersToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.IncludeSubFoldersToolStripMenuItem.Text = "Inclure les sous-dossiers";
            this.IncludeSubFoldersToolStripMenuItem.CheckedChanged += new System.EventHandler(this.IncludeSubFoldersToolStripMenuItem_CheckedChanged);
            // 
            // RunAtStartupToolStripMenuItem
            // 
            this.RunAtStartupToolStripMenuItem.CheckOnClick = true;
            this.RunAtStartupToolStripMenuItem.Name = "RunAtStartupToolStripMenuItem";
            this.RunAtStartupToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.RunAtStartupToolStripMenuItem.Text = "Démarrer avec Windows";
            this.RunAtStartupToolStripMenuItem.CheckedChanged += new System.EventHandler(this.RunAtStartupToolStripMenuItem_CheckedChanged);
            // 
            // CloseToolStripMenuItem
            // 
            this.CloseToolStripMenuItem.Image = global::NotifyMe.Properties.Resources.Close;
            this.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem";
            this.CloseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.CloseToolStripMenuItem.Text = "Fermer";
            this.CloseToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.ContextMenuStrip = this.NotifyIconContextMenuStrip;
            this.NotifyIcon.Visible = true;
            this.NotifyIcon.BalloonTipClicked += new System.EventHandler(this.NotifyIcon_BalloonTipClicked);
            this.NotifyIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_MouseDown);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 320);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.FileSystemWatcher)).EndInit();
            this.NotifyIconContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.FileSystemWatcher FileSystemWatcher;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
        private System.Windows.Forms.ContextMenuStrip NotifyIconContextMenuStrip;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
        private System.Windows.Forms.ToolStripMenuItem StartScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PauseScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator ToolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem SettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IncludeSubFoldersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RunAtStartupToolStripMenuItem;
    }
}

