﻿using System;
using System.IO;

namespace NotifyMe.Logging
{
    /// <summary>
    /// Représente le type de message d'un message du système de logs
    /// </summary>
    public enum LoggerMessageType
    {
        Information,
        Error,
        Debug
    };

    /// <summary>
    /// Représente le système de logs
    /// </summary>
    class Logger
    {

        #region Méthodes

        /// <summary>
        /// Initialise le système de logs
        /// </summary>
        /// <param name="filename"></param>
        public Logger(string filename)
        {
            writer = new StreamWriter(filename, true);

            // Permet le vidage de la mémoire-tampon dans le fichier logs
            writer.AutoFlush = true;
        }

        /// <summary>
        /// Ajoute une ligne dans le fichier logs
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        private void Write(string message, LoggerMessageType type)
        {
            writer.WriteLine("[{0}] {1} : {2}", type.ToString(), DateTime.Now, message);
        }

        /// <summary>
        /// Ajoute une ligne dans le fichier logs correspondant à une information
        /// </summary>
        /// <param name="message"></param>
        public void Information(string message)
        {
            Write(message, LoggerMessageType.Information);
        }

        /// <summary>
        /// Ajoute une ligne dans le fichier logs correspondant à une erreur
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            Write(message, LoggerMessageType.Error);
        }

        /// <summary>
        /// Ajoute une ligne dans le fichier logs correspondant à une information de débogage
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {

#if (DEBUG)

            Write(message, LoggerMessageType.Debug);

#endif

        }

        #endregion

        #region Propriétés

        /// <summary>
        /// Représente le fichier contenant les logs en écriture seule
        /// </summary>
        private StreamWriter writer;

        #endregion

    }
}
